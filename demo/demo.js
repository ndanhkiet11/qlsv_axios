// bất đồng bộ
setTimeout(function () {
    console.log(0);
}, 0);
setInterval(function () {
    console.log("hello");
}, 500);
// đồng bộ
console.log(1);
console.log(2);
// những đoạn code bất đồng bộ sẽ được thực thi sau cũng sau khi tất cả code đồng bộ chạy xong
