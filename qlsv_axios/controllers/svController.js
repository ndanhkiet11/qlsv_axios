function layThongTinTuForm() {
    // lấy thông tin từ user
    var maSv = document.getElementById("txtMaSV").value.trim();
    var tenSv = document.getElementById("txtTenSV").value.trim();
    var email = document.getElementById("txtEmail").value.trim();
    var matKhau = document.getElementById("txtPass").value.trim();
    var diemLy = document.getElementById("txtDiemLy").value.trim();
    var diemHoa = document.getElementById("txtDiemHoa").value.trim();
    var diemToan = document.getElementById("txtDiemToan").value.trim();

    // tạo object từ thông tin lấy từ form
    var sv = new SinhVien(
        maSv,
        tenSv,
        email,
        matKhau,
        diemLy,
        diemToan,
        diemHoa
    );
    return sv;
}

function renderDssv(list) {
    // render danh sách
    // contentHTML là 1 chuổi chứa các thẻ tr sau này sẽ innerHTML vào thẻ tbody
    var contentHTML = "";
    for (var i = 0; i < list.length; i++) {
        var currentSv = list[i];
        var cotentTr = `<tr> 
    <td>${currentSv.ma}</td>
    <td>${currentSv.ten}</td>
    <td>${currentSv.email}</td>
    <td>${currentSv.tinhDTB()}</td>
    <td>
    <button onclick="xoaSv('${
        currentSv.ma
    }')"  class="btn btn-danger">Xoá</button>

    <button
    
    onclick="suaSv('${currentSv.ma}')"
    class="btn btn-primary">Sửa</button>
    </td>
    </tr>`;
        contentHTML = contentHTML + cotentTr;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinLenForm(sinhVien) {
    document.getElementById("txtMaSV").value = sinhVien.ma;
    document.getElementById("txtTenSV").value = sinhVien.ten;
    document.getElementById("txtEmail").value = sinhVien.email;
    document.getElementById("txtPass").value = sinhVien.matKhau;
    document.getElementById("txtDiemLy").value = sinhVien.ly;
    document.getElementById("txtDiemHoa").value = sinhVien.hoa;
    document.getElementById("txtDiemToan").value = sinhVien.toan;
}

function resetForm() {
    // reset form
    document.getElementById("formQLSV").reset();
}
