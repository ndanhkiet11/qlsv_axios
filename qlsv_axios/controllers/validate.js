// hợp lệ true

function kiemTraRong(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerText =
      "Trường này không được để rỗng";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraMaSv(idSv, listSv, idError) {
  var index = listSv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  console.log("index: ", index);
  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Mã sinh viên đã tồn tại";
    return false;
  }
}

function kiemTraEmail(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  if (!isEmail) {
    
    document.getElementById(idError).innerText = "Email không hợp lệ";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

var value = "alicehahaha";
const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

var result = re.test(value);
console.log("result: ", result);
